#ifndef LISTNODE_H
#define LISTNODE_H

#include <iostream>

class listnode{
	private:
		void *data;
		listnode *next;

	public:
		void* getData();
		void setNext(listnode* toLink);
		listnode* getNext();
		listnode(void *toSave);
		~listnode();
};

#endif
