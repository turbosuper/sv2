#include "listnode.h"

void* listnode::getData(){
	return data;
}

listnode::listnode(void *toSave){
	data = toSave;
	next = NULL;
}

listnode::~listnode(){
	if(next != NULL){
		delete next;
	}
}

void listnode::setNext(listnode* toLink){
	next = toLink;
}

listnode* listnode::getNext(){
	return next;
}
