#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <iostream>
#include "listnode.h"

typedef int (*compare_f)(void* key1, void* key2);
// -------------------   linkedlist.cpp   --------------------------------------
class linkedlist{
	private:
		unsigned int items;
		listnode* first;
		listnode* tail;
		compare_f compFunc;

	public:
		bool addItem(void *data);
		unsigned int getItemCount();
		void* deleteItem(void *removeCompData);
		void* searchItem(void *searchKey);

		listnode* getFirst();
		listnode* getTail();
		

		linkedlist(compare_f UseToCompare);
		~linkedlist();
};
// -------------------   linkedlist.cpp   --------------------------------------

// -------------------   linkedlist_iterator.cpp   ------------------------------
class llIterator{
	private:
		linkedlist* list;
		listnode* current;

	public:
		bool hasNext();
		void* getNext();
		llIterator(linkedlist* listToIterate);
};
// -------------------   linkedlist_iteartor.cpp   ------------------------------
#endif
