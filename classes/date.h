#ifndef DATE_H
#define DATE_H
#include <iostream>
#include <string>
#include <sstream>

class date{
    private:
        int day;
        int month;
        int year;
    public:
        std::string getDate();
        void setDate(int d, int m, int y);
        int getDay();
        int getMonth();
        int getYear();
        date();
        date(int d, int m, int y);
};
#endif
