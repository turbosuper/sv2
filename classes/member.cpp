#include "member.h"

member::member(	std::string newFirstName,
		std::string newLastName,
		std::string newSex,
		std::string newAddress,
		std::string newEmail,
		int bDay,
		int bMonth,
		int bYear,
		bool newIsChairman,
		bool newIsActive
		)
		:birthday(bDay, bMonth, bYear){
	
	firstName = newFirstName;
	lastName = newLastName;
	sex = newSex;
	address	= newAddress;
	email = newEmail;
	isChairman = newIsChairman;
	isActive = newIsActive;
	points = 0;
}

void member::setFirstName(std::string newFirstName){
	firstName = newFirstName;
}

void member::setLastName(std::string newLastName){
        lastName = newLastName;
}

void member::setSex(std::string newSex){
	sex = newSex;
}

void member::setAddress(std::string newAddress){
	address = newAddress;
}

void member::setEmail(std::string newEmail){
	email = newEmail;
}

void member::setMemberIsActive(bool newStatus){
	isActive = newStatus;
}

void member::setMemberIsChairman(bool newStatus){
	isChairman = newStatus;
}

std::string member::getFirstName(){
	return firstName;
}

std::string member::getLastName(){
	return lastName;
}

std::string member::getSex(void){
	return sex;
}

std::string member::getAddress(void){
	return address;
}

std::string member::getEmail(void){
	return email;
}

date member::getBirtdayDate(void){
	return birthday;
}

std::string member::getBirthdayString(void){
	date ret_bday = birthday; // dummy copy of birthday to call getDate method in return statemant
	return ret_bday.getDate();
}

bool member::getChairmanStatus(void){
	return isChairman;
}

bool member::getActiveStatus(void){
	return isActive;
}

void member::setPoints(int newSumOfPoints){
	points = newSumOfPoints;
}

void member::addPoints(int pointstoadd){
	points += pointstoadd;
}

int member::getPoints(){
	return points;
}

