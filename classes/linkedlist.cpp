#include "linkedlist.h"

bool linkedlist::addItem(void *data){
	listnode* newnode = new listnode(data);
	if (newnode == NULL) return false;
	if (first == NULL){
		first = newnode;
	} else {
		tail->setNext(newnode);
	}	
	tail = newnode;
	items++;
	return true;
}

unsigned int linkedlist::getItemCount(){
	return items;
}

void* linkedlist::deleteItem(void *removeCompData){
	void* rmdata = NULL;
	if (items == 0){
		return rmdata; // returns NULL
	}
	listnode* last = NULL;
	listnode* node = first;
	while(node != NULL){
		int comp = compFunc(removeCompData, node->getData());
		if(comp == 0){ //found
			if(last == NULL){// first item should be deleted
				first = node->getNext();
			} else {
				last->setNext(node->getNext());// if this is the last node: get Next returns NULL and NULL will be set on the previous node as next
			}
			node->setNext(NULL); // prevent recursive delete
			rmdata = node->getData();
			delete node;
			node = NULL;
			items--;
		} else { // not found
			last = node;
			node = node->getNext();
		}
	}
	return rmdata;
}

void* linkedlist::searchItem(void *searchKey){
	if (items == 0){
		return NULL;
	}
	listnode* node = first;
	while(node != NULL){
		int comp = compFunc(searchKey, node->getData());
		if(comp == 0){ //found
			return node->getData();
		} else { // not found
			node = node->getNext();
		}
	}
	return NULL;
}

linkedlist::linkedlist(compare_f UseToCompare){
	items = 0;
	first = NULL;
	compFunc = UseToCompare;
}

linkedlist::~linkedlist(){
	if(items > 0){
		delete first;
	}
}

listnode* linkedlist::getFirst(){
	return first;
}

listnode* linkedlist::getTail(){
	return tail;
}
