#include "linkedlist.h"

bool llIterator::hasNext(){
	if(list == NULL) return false;
	return (current != NULL);
}

void* llIterator::getNext(){
	if(list == NULL) return NULL;
	if(hasNext() == false) return NULL;
	listnode* node = current;
	current = current->getNext();
	return node->getData();
}

llIterator::llIterator(linkedlist* listToIterate){
	list = listToIterate;
	current = list->getFirst();
}
