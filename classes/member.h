#ifndef MEMBER_H
#define MEMBER_H

#include "date.h"
#include <iostream>
#include <string>

class member{
	private:
		std::string firstName;
		std::string lastName;
		std::string sex;
		std::string address;
		std::string email;
		const date birthday;
		bool isChairman;
		bool isActive;
		int points;
		
	public:
		void setFirstName(std::string newFirstName);
		void setLastName(std::string newLastName);
		void setSex(std::string newSex);
		void setAddress(std::string newAddress);
		void setEmail(std::string newEmail);
		void setMemberIsActive(bool newStatus);
		void setMemberIsChairman(bool newStatus);
		std::string getFirstName(void);
		std::string getLastName(void);
		std::string getSex(void);
		std::string getAddress(void);
		std::string addMember(void);
		std::string getEmail(void);
		date getBirtdayDate(void);
		std::string getBirthdayString(void);
		bool getChairmanStatus(void);
		bool getActiveStatus(void);
		void setPoints(int newSumOfPoints);
		void addPoints(int pointstoadd);
		int getPoints(void);

		member(
			std::string firstName,
			std::string lastName,
			std::string sex,
			std::string address,
			std::string email,
			int bDay,
			int bMonth,
			int bYear,
			bool isChairman,
			bool isActive
		);
};


#endif
