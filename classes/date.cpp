#include "date.h"

date::date(){
    day = 0;
    month = 0;
    year = 0;
}
date::date(int d, int m, int y){
    day = d;
    month = m;
    year = y;
}
std::string date::getDate(){
	std::ostringstream d;
	std::ostringstream m;
	std::ostringstream y;
	d << day;
	m << month;
	y << year;
	std::string output = d.str() + '.' + m.str() + '.' + y.str();
	return output;
}
void date::setDate(int d, int m, int y){
    day = d;
    month = m;
    year = y;
}
int date::getDay(){return day;}
int date::getMonth(){return month;}
int date::getYear(){return year;}

