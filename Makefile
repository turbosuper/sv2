CC = "g++"

CDIR = ./classes
FDIR = ./functions
CPPFLAGS= -g -Wall -I${CDIR} -I${FDIR}

EXE = sv2
OBJS = ${EXE}.o
COBJS = ${CDIR}/*.o
FOBJS = ${FDIR}/*.o

${EXE}: ${OBJS} class_obj func_obj
	${CC} -o $@ ${OBJS} ${COBJS} ${FOBJS}

${OBJS}: ${CDIR}/*.h ${FDIR}/*.h


class_obj:
	cd ${CDIR}; ${MAKE} all "CC=${CC}" "CPPFLAGS=${CPPFLAGS}"

func_obj:
	cd ${FDIR}; ${MAKE} all "CC=${CC}" "CPPFLAGS=${CPPFLAGS}"

run: ${EXE}
	./${EXE}

clean:
	/bin/rm -f core *.o; cd ${CDIR}; ${MAKE} clean; cd ../${FDIR}; ${MAKE} clean
