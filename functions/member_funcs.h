#ifndef MEMBER_FUNCS_H
#define MEMBER_FUNCS_H

#include <iostream>
#include <string>
#include "../classes/member.h"
#include "../classes/linkedlist.h"
#include "input_funcs.h"


member* addMember(linkedlist* memberlist);
int findMember(void* compData1, void* compData2);
void deleteMember(linkedlist* meberlist);
void printMemberInfo(member* ptrMember);

#endif
