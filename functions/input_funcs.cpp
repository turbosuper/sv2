#include "input_funcs.h"

void getUserInput(int &inputInt){

	std::string inputString;
	std::stringstream buffer;
	while(1){
		std::getline(std::cin, inputString);

	

		buffer << inputString;
		buffer >> inputInt;
		if(buffer.fail()){
			std::cout << "\nUngültige Eingabe! Bitte einen Integer eingeben:" << std::endl;
			buffer.str(std::string());
			buffer.clear();
		}else{
			break;
		}
	}
	

	return;
}

void getUserInput(std::string &userInput){

	std::string inputString;
	do{
		std::getline(std::cin, inputString);
	}while(inputString == "");

	userInput = inputString;

}

void getUserInput(int &day, int &month, int &year){

	std::string inputString, token;
	std::stringstream buffer, ss;
	while(1){
		std::getline(std::cin, inputString);
		buffer << inputString;

		std::getline(buffer, token, '.');
		ss << token;
		ss >> day;
		
		ss.clear();
		token = "";

		std::getline(buffer, token, '.');
		ss << token;
		ss >> month;

		ss.clear();
		token = "";

		std::getline(buffer, token);
		ss << token;
		ss >> year;

		if(ss.fail()){
			std::cout << "Buffer failed!" << std::endl;
			std::cout << "Day = " << day << std::endl;
			std::cout << "Month = " << month << std::endl;
			std::cout << "Year = " << year << std::endl;
			std::cout << "\nUngültige Eingabe! Bitte ein Datum eingeben:" << std::endl;
		}else{
			if(day < 1 || day > 31){
				std::cout << "Ungültige Eingabe! Der Tag muss zwischen 1 und 31 liegen!" << std::endl;
			}
			else if(month < 1 || month > 12){
				std::cout << "Ungültige Eingabe! Der Monat muss zwischen 1 und 12 liegen!" << std::endl;
			}else if(year < 1900 || year > 2100){
				std::cout << "Ungültige Eingabe! Das Jahr muss zwischen 1900 und 2100 liegen!" << std::endl;
			}else{
				break;
			}
		}
		
		buffer.str(std::string());
		buffer.clear();
		ss.str(std::string());
		ss.clear();
	}
}