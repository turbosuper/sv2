#ifndef MENUFUNCS_H
#define MENUFUNCS_H

#include <iostream>
#include <string>
#include <sstream>
#include <limits>
#include "../classes/member.h"
#include "../classes/linkedlist.h"
#include "member_funcs.h"
#include "input_funcs.h"

void showMenu();
void StatusMenu(member *member);
void manageStatus(linkedlist* memberlist);
void memberInfo(linkedlist* memberlist);
void printAllMemberInfo(linkedlist* memberlist);
void memberPoints(linkedlist* memberlist);

#endif
