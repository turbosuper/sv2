#include "member_funcs.h"

int findMember(void* compData1, void* compData2){	
	member* member1 = (member*)compData1;
	member* member2 = (member*)compData2;
	// combine name of member1

	std::string fname = member1->getFirstName();
	std::string lname = member1->getLastName();
	std::string fullName1 = fname + ' ' + lname;

	// combine name of meber2

	fname = member2->getFirstName();
	lname = member2->getLastName();
	std::string fullName2 = fname + ' ' + lname;

	// check fullName's
	
	return fullName1.compare(fullName2);
}

member* addMember(linkedlist* memberlist){
	member* ptrNewMember = NULL;
	std::string question1, question2, question3;
	std::string fName;
	std::string lName;
	std::string fsex;
	std::string fadresse;
	std::string femail;
	int Day, Month, Year;
	bool isChairman, isActive;

	do{
		std::cout << "Bitte geben sie den Vornamen ein: ";
		getUserInput(fName);
		std::cout << "Bitte geben sie den Nachnamen ein: " ;
		getUserInput(lName);
		std::cout << "Bitte geben sie das Geschlecht ein: " ;
		getUserInput(fsex);
		std::cout << "Bitte geben sie das Geburtsdatum ein [TT.MM.JJJJ]: " ;
		getUserInput(Day, Month, Year);
		std::cout << "Bitte geben sie die Adresse ein: " ;
		getUserInput(fadresse);
		std::cout << "Bitte geben sie die E-Mail Adresse ein: " ;
		getUserInput(femail);
		std::cout << "Ist das Mitglied Teil des Verstandes (j)a, (N)ein: " ;
		getUserInput(question2);
			if (question2 != "j" && question2 != "J") isChairman = 1;
			else  isChairman = 0;			
		std::cout << "Ist Mitglied aktiv (j)a, (N)ein: " ;
		getUserInput(question3);
			if (question3 != "j" && question3 != "J") isActive = 1 ;
			else isActive = 0;			


		ptrNewMember = new member(fName, lName, fsex, fadresse, femail, Day, Month, Year, isChairman, isActive);

		printMemberInfo(ptrNewMember);

		std::cout << "\nSind diese Daten richtig? (j)a, (n)ein: " ;
		getUserInput(question1);
		if (question1 != "j" && question1 != "J"){
			delete ptrNewMember;
		}

	}while (question1 != "j" && question1 != "J");
	memberlist->addItem(ptrNewMember);
	return ptrNewMember;
}

void deleteMember(linkedlist* meberlist){
	std::cout << "Löschen eines Mitglieds:" << std::endl;
	std::cout << "Welches Mitglied soll entfernt werden?\n"
			"Vorname: ";
	std::string fname, lname;
	getUserInput(fname);
	std::cout << "Nachname: ";
	getUserInput(lname);
	member searchKey(fname, lname, "", "", "", 0, 0, 0, false, false);
	member *delmemb = (member*)meberlist->deleteItem(&searchKey);
	if(delmemb == NULL){
		std::cerr << "Mitglied wurde nicht gefunden und / oder konnte nicht gelöscht werden!" << std::endl;
		return;
	}
	printMemberInfo(delmemb);
	delete delmemb;
	std::cout << "Das Mitglied wurde entfernt!" << std::endl;
	std::cout << std::endl << std::endl << "----------------------------------------" << std::endl << std::endl;
}

void printMemberInfo(member* ptrMember){

	if (ptrMember == NULL)
	{
		std::cerr << "Das Mitglied existiert nicht!" << std::endl;
		return;
	}

	std::cout << ptrMember->getFirstName() << " " << ptrMember->getLastName() << ", " << ptrMember->getSex() << std::endl;
	std::cout << ptrMember->getBirthdayString() << std::endl;
	std::cout << ptrMember->getAddress() << std::endl;
	std::cout << ptrMember->getEmail() << std::endl;
	if(ptrMember->getActiveStatus()){
		std::cout << "Aktives Mitglied";
	}else{
		std::cout << "Inaktives Miglied";
	}

	std::cout << " mit " << ptrMember->getPoints() << " Punkten." << std::endl;

	if(ptrMember->getChairmanStatus()){
		std::cout << "Teil des Vorstands." << std::endl;
	}else{
		std::cout << "Kein Teil des Vorstands." << std::endl;
	}

	std::cout << std::endl;
}
