#include "menu_funcs.h"

void StatusMenu(member *member){
	std::string name = member->getFirstName() + ' ' + member->getLastName();
	std::string ans = "x";
	if(member->getActiveStatus()){
		std::cout << name << " ist als Aktiv makiert.\n"
		"Möchten sie " << name << " als inaktiv setzen? [y oder n]" << std::endl;
		getUserInput(ans);
		if(ans == "y"){
			member->setMemberIsActive(false);
			std::cout << "Alter Status: AKTIV | Neuer Staus NICHT AKTIV" << std::endl;
		} else if(ans == "n"){
			std::cout << "Status verbleibt bei AKTIV" << std::endl;
		} else {
			std::cerr << "Keine gültige eingabe y oder n!" << std::endl;
		}
	} else {
		std::cout << name << " ist als nicht Aktiv markiert.\n"
		"Möchten sie " << name << " als aktiv setzen? [y oder n]" << std::endl;
		getUserInput(ans);
		if(ans == "y"){
			member->setMemberIsActive(true);
			std::cout << "Alter Status: NICHT AKTIV | Neuer Status AKTIV" << std::endl;
		} else if(ans == "n"){
			std::cout << "Status verbleibt bei NICHT AKTIV" << std::endl;
		} else {
			std::cerr << "Keine gültige eingabe y oder n!" << std::endl;
		}
	}
}

void showMenu(){
	std::cout << 	"\n---------------------------Hauptmenü---------------------------\n"
			"Bitte geben sie eine Zahl für eine der folgenden Optionen ein:\n"
			"[0] Beenden dieses Programms\n"
			"[1] Neues Mitglied hinzufügen\n"
			"[2] Mitglieder anzeigen\n"
			"[3] Mitglied aus der Liste entfernen\n"
			"[4] Status für ein Mitglied bearbeiten\n"
			"[5] Punkte für Mitglied hinzufügen\n" << std::endl;
}

void manageStatus(linkedlist* memberlist){
	std::cout << "\nStatus eines Mitglieds ändern:" << std::endl;
	std::cout << "Welches Mitglied soll bearbeitet Werden? Bitte geben sie den Namen ein\n"
			"Format [Vorname Nachname]: ";
	std::string fname;
	std::string lname;
	std::cin >> fname >> lname;
	member searchKey(fname, lname, "", "", "", 0, 0, 0, false, false);
	member *fmember = (member*)memberlist->searchItem(&searchKey);
	if(fmember == NULL){
		std::cerr << "Mitglied konnte nicht gefunden werden!" << std::endl;
		return;
	}
	StatusMenu(fmember);
}

void memberInfo(linkedlist* memberlist){
	int userInput;

	if(memberlist == NULL){
		std::cerr << "No valid member list!\n" << std::endl;
		return;
	}

	std::cout << "\n--------------Mitglied Ausgabe--------------\n"
			"Auswahlmöglichkeiten:\n"
			"[0] Verlassen und zurück zum Hauptmenü\n"
			"[1] Alle Mitglieder anzeigen\n"
			"[2] Mitglied nach Namen suchen/anzeigen\n"
			"[3] Anzahl der Mitglieder anzeigen lassen\n" << std::endl;
		do{
			getUserInput(userInput);
			switch(userInput){
				case 0:
				{
					break;
				}
				case 1:
				{
					printAllMemberInfo(memberlist);
					break;
				}
				case 2:
				{
					std::cout << "\nBitte den vollen Namen eingeben\n"
								 "Format [Vorname Nachname]: ";
					std::string fName, lName;
					std::cin >>  fName >> lName;
					std::cout << std::endl;
					member searchKey(fName, lName, "", "", "", 0, 0, 0, false, false);
					member* findMember = (member*)memberlist->searchItem(&searchKey);
					if(findMember == NULL){
						std::cout << "Das Mitglied konnte nicht gefunden werden!" << std::endl;
						break;
					}
					printMemberInfo(findMember);
					break;
				}
				case 3:
				{
					std::cout << "Es sind " << memberlist->getItemCount() << " Miglieder gespeichert.\n" << std::endl;
					break;
				}
				default:
				{
					std::cerr << "Keine gültige eingabe! Bitte eingabe wiederholen!" << std::endl;
				}
			}
		}while(userInput < 0 || userInput > 3);

};

void printAllMemberInfo(linkedlist* memberlist){

	llIterator iterator(memberlist);
	while(iterator.hasNext()){
		member* ptrMember = (member*)iterator.getNext();
		printMemberInfo(ptrMember);
	}
}

void memberPoints(linkedlist* memberlist){

	std::cout << "Punkte für Mitglied hinzufügen:" << std::endl;
	std::cout << "Welchem Mitglied möchten Sie Punkte geben? Bitte geben sie den Namen ein\n"
			"Format [Vorname Nachname]: ";
	std::string fname, lname;
	std::cin >> fname >> lname;
	member searchKey(fname, lname, "", "", "", 0, 0, 0, false, false);
	member *fmember = (member*)memberlist->searchItem(&searchKey);
	if(fmember == NULL){
		std::cerr << "Mitglied konnte nicht gefunden werden!" << std::endl;
		return;
	}
	
	printMemberInfo(fmember);

	std::string userInput;
	int pointsToAdd;
	std::cout << "Sind Sie sicher das Sie diesem Mitlgied Punkte hinzufügen möchten? (y/n)" << std::endl;
	std::cin >> userInput;
	std::cout << std::endl;

	if (userInput != "y") return;

	std::cout << "Wie viele Punkte wollen Sie diesem Mitglied hinzufügen?" << std::endl;
	std::cin >> pointsToAdd;

	fmember->addPoints(pointsToAdd);
	std::cout << "Dieses Mitglied hat nun " << fmember->getPoints() << " Punkte!" << std::endl;

}
