#ifndef INPUTFUNCS_H
#define INPUTFUNCS_H

#include <iostream>
#include <string>
#include <sstream>

void getUserInput(int &userInput);
void getUserInput(std::string &userInput);
void getUserInput(int &day, int &month, int &year);

#endif