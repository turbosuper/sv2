#include <string>
#include <iostream>
#include <limits>
#include <cstdlib>
#include "classes/member.h"
#include "classes/linkedlist.h"
#include "functions/menu_funcs.h"
#include "functions/member_funcs.h"

using namespace std;

void createDemoData(linkedlist* memberlist);

int main(){

	linkedlist memberlist(findMember);

	createDemoData(&memberlist); // only until programm reads / writes from files
	int choice = 99;
	system("clear");
	cout << "Sportverein Manager 2" << endl;
	do{
		showMenu();
		getUserInput(choice);
		switch(choice){
		case 0:
			{
				cout << "Programm wird normal beendet" << endl;
				break;
			}
		case 1:
			{
				system("clear");
				if(addMember(&memberlist)) cout << "Mitglied erfolgreich gespeichert" << endl;
				break;
			}
		case 2:
			{
				memberInfo(&memberlist);
				break;
			}
		case 3:
			{
				deleteMember(&memberlist);
				break;
			}
		case 4:
			{
				manageStatus(&memberlist);
				break;
			}
		case 5:
			{
				memberPoints(&memberlist);
				break;
			}
		default:
			{
				cerr << "Keine gültige eingabe!" << endl;
				cout << endl << endl << "----------------------------------------" << endl << endl;
			}
		}
	} while(choice);
	return 0;
}

void createDemoData(linkedlist* memberlist){ // Only for testing until programm reads / writes to files
	member* memb1 = new member("Max", "Mustermann", "male", "Berliner Alle 122, 10365 Berlin", "mmuster@irgenwo.creative", 1, 3, 1968, true, true);
	member* memb2 = new member("Erika", "Mustermann", "female", "Berliner Alle 122, 10365 Berlin", "emuster@irgenwo.creative", 24, 11, 1966, false, true);
	member* memb3 = new member("Irgend", "Jemand", "male", "Potzdammer Platz 1, 11234 Berlin", "jemand@demo-domain.de", 26, 9, 1996, false, false);
	if(memberlist->addItem(memb1) == false) cerr << "Failed to create demo member 1\n";
	if(memberlist->addItem(memb2) == false) cerr << "Failed to create demo member 2\n";
	if(memberlist->addItem(memb3) == false) cerr << "Failed to create demo member 3\n";
}
