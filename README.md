# sv2

This is a school project aiming to create a basic management program for a club.

Compile
```
make
```

Compile (if needed) and run
```
make run
```

Clean project folder by deleting object files
```
make clean
```
